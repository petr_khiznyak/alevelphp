<?php
session_start();
if ($_POST['submit']) {
  if (!empty($_POST['name']) && !empty($_POST['comm'])) {
    print_r($_SESSION['comments'] = [
      'name' => $_POST['name'],
      'comm' => $_POST['comm']
    ]);
  } else {
    echo 'Some fields is empty';
  }
}

?>
<style>
  form{
      display: flex;
      flex-direction: column;
  }
  input {
      margin-top: 5px;
  }
</style>

<form action="<?= $_SERVER['SCRIPT_NAME']?>" method="post">
  <label for="name">Name</label>
  <input type="text" name="name" id="name">
  <label for="comm">Comment</label>
  <textarea name="comm" id="comm" cols="30" rows="10"></textarea>
  <input type="submit" value="Submit" name="submit">
  <input type="reset" value="Reset">
</form>
