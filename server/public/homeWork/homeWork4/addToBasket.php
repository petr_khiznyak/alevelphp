<?php
session_start();
if ($_POST['submit']) {
  if (!empty($_POST['id']) && !empty($_POST['amount'])) {
    print_r($_SESSION['addToBasket'] = [
      'id' => $_POST['id'],
      'amount' => $_POST['amount']
    ]);
  } else {
    echo 'Some fields is empty';
  }
}

?>
<style>
    form{
        display: flex;
        flex-direction: column;
    }
    input {
        margin-top: 5px;
    }
</style>

<form action="<?= $_SERVER['SCRIPT_NAME']?>" method="post">
  <label for="id">Product</label>
  <input type="text" name="id" id="id">
  <label for="amount">Amount</label>
    <input type="number" name="amount" id="amount">
  <input type="submit" value="Submit" name="submit">
  <input type="reset" value="Reset">
</form>
