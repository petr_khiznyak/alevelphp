<?php
session_start();
if ($_POST['submit']) {
  if (!empty($_POST['name']) && !empty($_POST['phoneNumber'])) {
    print_r($_SESSION['callBack'] = [
      'name' => $_POST['name'],
      'phoneNumber' => $_POST['phoneNumber']
    ]);
  } else {
    echo 'Some fields is empty';
  }
}

?>
<style>
    form{
        display: flex;
        flex-direction: column;
    }
    input {
        margin-top: 5px;
    }
</style>

<form action="<?= $_SERVER['SCRIPT_NAME']?>" method="post">
  <label for="name">Name</label>
  <input type="text" name="name" id="name">
  <label for="phoneNumber">Phone number</label>
  <input type="tel" name="phoneNumber" id="phoneNumber">
  <input type="submit" value="Submit" name="submit">
  <input type="reset" value="Reset">
</form>

